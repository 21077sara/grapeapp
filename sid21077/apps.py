from django.apps import AppConfig


class Sid21077Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sid21077'
