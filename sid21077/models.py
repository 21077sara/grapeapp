from django.db import models

from django.urls import reverse

# Create your models here.

class ModuleGroup(models.Model):
    code = models.CharField(max_length=10)
    label = models.CharField(max_length=225)
    semestre = models.CharField(max_length=45,default="S2")
    department = models.CharField(max_length=45,default="SID")
# Methods
    def get_absolute_url(self):
       return reverse('index')
class Module(models.Model):
    code = models.CharField(max_length=10)
    label = models.CharField(max_length=255)
    coef = models.IntegerField()
    modulegroupecode = models.CharField(max_length=255)
    # Methods
    def get_absolute_url(self):
        return reverse('index')   

class grade(models.Model):
    identifiant = models.IntegerField()
    note_sn = models.FloatField(max_length=255)
    note_sc = models.FloatField(max_length=255)
    year = models.IntegerField()
    module_code = models.CharField(max_length=10)
    student_id = models.CharField(max_length=255)
    # Methods
    def get_absolute_url(self):
        return reverse('index' ) 