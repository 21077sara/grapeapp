from django.urls import path
from . import views

urlpatterns = [
    path('', views.home , name= 'index' ),
    path('hello',views.hello,name='hello'),
    path('modulegroup/add',views.ModuleGroupCreate.as_view(),name='modulegroup'),
    path('modulegroup/list', views.ModuleGroupList.as_view() , name= 'modulegroup-list' ),
    path('module/add',views.ModuleCreate.as_view(),name='module'),
    path('module/list', views.ModuleList.as_view() , name= 'module-list' ),
    path('grade/add',views.gradeCreate.as_view(),name='grade'),
    path('grade/list', views.ModuleGroupList.as_view() , name= 'grade-list' ),
    path('student/add',views.studentCreate.as_view(),name='student'),
    path('module_has_student/add',views.module_has_studentCreate.as_view(),name='module_has_student'),
]      