from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import CreateView , ListView
from .models import ModuleGroup ,Module, grade, module_has_student, student 
# Create your views here.
def home(request):     
    return HttpResponse("Hello, world. You're at the ESP grades app!")

def hello(request):   
    vparm=request.GET['var1']
    vparm2=request.GET['var2']
    return render(request,'templetes/index.html',{'var1':vparm,'bar2':vparm2})    
# CRUD
class ModuleGroupCreate(CreateView): 
    model=ModuleGroup
    fields='__all__'  

class ModuleGroupList(ListView):
    model = ModuleGroup   

class ModuleCreate(CreateView): 
    model=Module
    fields='__all__'  

class ModuleList(ListView):
    model = Module              

class gradeCreate(CreateView): 
    model= grade
    fields='__all__'  

class gradeList(ListView):
    model = grade  

class studentCreate(CreateView): 
    model= student
    fields='__all__'  

class studentList(ListView):
    model = student 

class module_has_studentCreate(CreateView): 
    model= module_has_student
    fields='__all__'  

class module_has_studentList(ListView):
    model = module_has_student  